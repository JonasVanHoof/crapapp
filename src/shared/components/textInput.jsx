import React, { Fragment } from "react";
import { validationTypes } from "../../config";

const TextInput = ({ handler, touched, hasError, meta }) => {
    return (
        <Fragment>
            <div className="row">
                <div className="input-field col s12">
                    <input id={meta.label} type="text" {...handler()} />
                    <label htmlFor={meta.label}>{meta.label.toUpperCase()}</label>
                </div>
            </div>
            <div>
                <span className="warning">
                    {touched
                        && hasError(validationTypes.REQUIRED)
                        && `${meta.label} is required`
                        || hasError(validationTypes.PATTERN)
                        && `Insert a valid ${meta.label.toLowerCase()}`
                    }
                </span>
            </div>
        </Fragment>
    );
}

export default TextInput;