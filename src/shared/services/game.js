import { apiUrls, authenticationToken } from "../../config";

export function getSetup() {
    return fetch(apiUrls.game, {
        headers: {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${window.sessionStorage.getItem(authenticationToken)}`
        }
    });
}

export function registerShoot(id, rowValue, colValue) {
    return fetch(`${apiUrls.game}${id}/move`, {
        method: 'POST',
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : `Bearer ${window.sessionStorage.getItem(authenticationToken)}`
         },
         body: JSON.stringify({rowValue: rowValue, columnValue: colValue})
    });
}