import { apiUrls } from "../../config";

export function sendMailUsername(email){

    return fetch(apiUrls.mailForgotUsername, {
        method: "POST",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(email)
    })
}

